# javatest

Changes that have been made:

- Removed spring web
- Removed a lot of unused dependencies from pom and added a parent pom to spring boot so that we don't need to manage versioning.
- Replaced with spring boot. This approach makes the service cloud-native and easier to deploy anywhere as the web server is packaged with the service.
- Moved to constructor injected dependencies. This generally makes it easier to test and inject mocks.
- Moved to Annotation-based configuration. Widely preferred to xml-based configuration.
- Removed unneeded classes (SmartBD). This class wasn't adding any value, just proxying requests to the service.
- Integration and unit tests added
- Used Spring JPA with in-memory HSQL db for storage
- Added mapped exceptions for 404 Not Found when a meter reading is not found
