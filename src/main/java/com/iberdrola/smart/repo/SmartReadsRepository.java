package com.iberdrola.smart.repo;

import com.iberdrola.smart.model.SmartReads;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SmartReadsRepository extends JpaRepository<SmartReads, Long> {

}
