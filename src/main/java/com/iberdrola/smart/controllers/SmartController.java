package com.iberdrola.smart.controllers;

import com.iberdrola.smart.exceptions.ReadingsNotFoundException;
import com.iberdrola.smart.model.SmartReads;
import com.iberdrola.smart.service.SmartService;
import org.apache.catalina.connector.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class SmartController {
    private final SmartService smartService;

    private static Log LOG = LogFactory.getLog(SmartController.class);

    public SmartController(SmartService smartService) {
        this.smartService = smartService;
    }

    @RequestMapping(value = "/getsmartreads/{custid}", method = RequestMethod.GET, produces = {"application/json"})
    public SmartReads getSmartReads(@PathVariable("custid") Long custID) {

        LOG.debug("getsmartreads - Request for: " + custID);

        return smartService.getSmartReads(custID).orElseThrow(ReadingsNotFoundException::new);
    }
}
