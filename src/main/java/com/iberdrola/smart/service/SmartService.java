package com.iberdrola.smart.service;

import com.iberdrola.smart.model.SmartReads;

import java.util.Optional;

public interface SmartService {
	Optional<SmartReads> getSmartReads(Long custID);
}
