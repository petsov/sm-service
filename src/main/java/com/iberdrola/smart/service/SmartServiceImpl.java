package com.iberdrola.smart.service;

import com.iberdrola.smart.model.SmartReads;
import com.iberdrola.smart.repo.SmartReadsRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Optional;

public class SmartServiceImpl implements SmartService {
    private final SmartReadsRepository readsRepository;

    public SmartServiceImpl(SmartReadsRepository readsRepository) {
        this.readsRepository = readsRepository;
    }

    @Override
    public Optional<SmartReads> getSmartReads(Long custID) {
        return readsRepository.findById(custID);
    }
}
