package com.iberdrola.smart;

import com.iberdrola.smart.repo.SmartReadsRepository;
import com.iberdrola.smart.service.SmartService;
import com.iberdrola.smart.service.SmartServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmartAppConfig {
    @Bean
    public SmartService getSmartService(SmartReadsRepository readsRepository) {
        return new SmartServiceImpl(readsRepository);
    }

}
