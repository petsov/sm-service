package com.iberdrola.smart.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "smart_reads")
public class SmartReads implements Serializable {

	//Database field names:
	//ACCOUNT_NUMBER
	//GAS_ID
	//ELEC_ID
	//ELEC_SMART_READ
	//GAS_SMART_READ

	@Id
	@GeneratedValue
	@Column(name="ACCOUNT_NUMBER",nullable = false)
    private Long accountNumber;
	@Column(name="GAS_ID")
    private String gasID;
	@Column(name="ELEC_ID")
    private String elecID;
	@Column(name="ELEC_SMART_READ")
	private String elecSmartRead;
	@Column(name="GAS_SMART_READ")
    private String gasSmartRead;

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getGasID() {
		return gasID;
	}

	public void setGasID(String gasID) {
		this.gasID = gasID;
	}

	public String getElecID() {
		return elecID;
	}

	public void setElecID(String elecID) {
		this.elecID = elecID;
	}

	public String getElecSmartRead() {
		return elecSmartRead;
	}

	public void setElecSmartRead(String elecSmartRead) {
		this.elecSmartRead = elecSmartRead;
	}

	public String getGasSmartRead() {
		return gasSmartRead;
	}

	public void setGasSmartRead(String gasSmartRead) {
		this.gasSmartRead = gasSmartRead;
	}
}
