package com.iberdrola.smart.service;

import com.iberdrola.smart.model.SmartReads;
import com.iberdrola.smart.repo.SmartReadsRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class SmartServiceImplTest {
    @Mock
    private SmartReadsRepository smartReadsRepository;
    private SmartService service;
    @Mock
    private SmartReads result;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        service = new SmartServiceImpl(smartReadsRepository);
        when(smartReadsRepository.findById(1L)).thenReturn(Optional.of(result));
    }

    @Test
    public void getSmartReads() {
        assertEquals(result, service.getSmartReads(1L).get());
    }
}