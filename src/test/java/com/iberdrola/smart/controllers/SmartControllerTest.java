package com.iberdrola.smart.controllers;

import com.iberdrola.smart.exceptions.ReadingsNotFoundException;
import com.iberdrola.smart.model.SmartReads;
import com.iberdrola.smart.service.SmartService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class SmartControllerTest {
    @Mock
    private SmartService service;
    @Mock
    private SmartReads result;
    private SmartController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        this.controller = new SmartController(service);
        when(service.getSmartReads(1L)).thenReturn(Optional.of(result));
        when(service.getSmartReads(123L)).thenReturn(Optional.empty());
    }

    @Test
    public void getSmartReads() {
        assertEquals(result, controller.getSmartReads(1L));
    }

    @Test(expected = ReadingsNotFoundException.class)
    public void getSmartReadsNull() {
        controller.getSmartReads(123L);
    }
}