package com.iberdrola.webclish;

import com.iberdrola.smart.SmartApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SmartApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SmartIT {
    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/getsmartreads/1");
    }

    @Test
    public void getSmartReadings() {
        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);
        assertEquals(response.getBody(), "{\"accountNumber\":1,\"gasID\":\"21321\",\"elecID\":\"GasId1\",\"elecSmartRead\":\"ElecId1\",\"gasSmartRead\":\"3213\"}");
    }

}
